from tkinter import *
from tkinter import messagebox
from tkinter import simpledialog

from game import bloquear_botones_gato, verificar_ganador, Turnos

ventana=Tk()    #Con la biblioteca tkinter abrimos una ventana para hacer el juedo de gato
ventana.geometry("400x450")     #El tamaño de la ventana
ventana.title("Gato")           #El título
turno_siguiente=0               #Nos ayuda a controlar el turno
Jugador1= ""
Jugador2= ""
lista_De_Botones=[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
tablero=[' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']    # 3 posibilidades: X,O, N   N= Nada en el tablero
turno_delJugador = StringVar()

class Gato:
    def __init__(self, Jugador1, Jugador2):
        self.Jugador1 = Jugador1
        self.Jugador2 = Jugador2

    def bloquear_botones_gato(self):  # Se desabilitan los botones en caso de que no se haya iniciado el juego de gato
        for i in range(0, 9):
            lista_De_Botones[i] #.config(state="disable")


    def iniciarJuego(self):  # Una vez que se le click al boton de iniciar juego, los demás botones para el gato podrán usarse
        for i in range(0, 9):
            lista_De_Botones[i] #.config(state="normal")
            lista_De_Botones[i] #.config(bg="orange")
            lista_De_Botones[i] #.config(text="")  # Para que en siguientes juegos, estén vacíos los botones
            tablero[i] #= ['N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N']
        global Jugador1, Jugador2
        self.Jugador1 = simpledialog.askstring("Jugador","¿Cuál es tu nombre P1?:")  # Se utiliza la bilioteca simpledialog para mostrar una ventana
        self.Jugador2 = simpledialog.askstring("Jugador","¿Cuál es tu nombre P2?:")  # donde se pregunte el nombre de ambos jugadores
        turno_delJugador.set("\tTurno de: " + self.Jugador1)  # Muestra el primer jugador (el nombre)


    def Turnos(self,num):  # En esta funcion se va a llevar el control de los turnos de cada jugador
        global turno_siguiente, Jugador1, Jugador2
        if tablero[num] == "N" and turno_siguiente == 0:
            lista_De_Botones[num] #.config(text="X")  # Cuando el primero jugador seleccione una opcion en el gato esta se pondrá una X
            tablero[num] = "X"
            turno_siguiente = 1  # En esta parte le toca de nuevo al jugador numero 2
            turno_delJugador.set("\tTURNO DE: " + self.Jugador2)
        elif tablero[num] == "N" and turno_siguiente == 1:
            lista_De_Botones[num] #.config(text="O")  # Cuando el segundo jugador seleccione una opcion en el gato esta se pondrá una O
            tablero[num] = "O"
            turno_siguiente = 0  # En esta parte le toca de nuevo al jugador numero 1
            turno_delJugador.set("\tTURNO DE: " + self.Jugador1)  # Muestra un mensaje en la ventana del siguiente jugador
        lista_De_Botones[num] #.config(state="disable")  # En caso de que los usuarios quieran dar click en un recuadro que ya a sido clickeado
        verificar_ganador()


    def verificar_ganador(self):
        # Jugador 1 opciones en las que puede ganar
        if (tablero[0] == "X" and tablero[1] == "X" and tablero[2] == "X") or (tablero[3] == "X" and tablero[4] == "X" and tablero[5] == "X") or (tablero[6] == "X" and tablero[7] == "X" and tablero[8] == "X"):
            bloquear_botones_gato()
            messagebox.showinfo("Ganador", "EL GANADOR ES: " + self.Jugador1)
        elif (tablero[0] == "X" and tablero[3] == "X" and tablero[6] == "X") or (tablero[1] == "X" and tablero[4] == "X" and tablero[7] == "X") or (tablero[2] == "X" and tablero[5] == "X" and tablero[8] == "X"):
            bloquear_botones_gato()
            messagebox.showinfo("Ganador", "EL GANADOR ES: " + self.Jugador1)
        elif (tablero[0] == "X" and tablero[4] == "X" and tablero[8] == "X") or (tablero[2] == "X" and tablero[4] == "X" and tablero[6] == "X"):  # Hasta aqui opciones jugador 1
            bloquear_botones_gato()
            messagebox.showinfo("Ganador", "EL GANADOR ES: " + self.Jugador1)

        # Jugador 2 opciones en las que puede ganar
        elif (tablero[0] == "O" and tablero[1] == "O" and tablero[2] == "O") or (tablero[3] == "O" and tablero[4] == "O" and tablero[5] == "O") or (tablero[6] == "O" and tablero[7] == "O" and tablero[8] == "O"):
            bloquear_botones_gato()
            messagebox.showinfo("Ganador", "EL GANADOR ES: " + self.Jugador2)
        elif (tablero[0] == "O" and tablero[3] == "O" and tablero[6] == "O") or (tablero[1] == "O" and tablero[4] == "O" and tablero[7] == "O") or (tablero[2] == "O" and tablero[5] == "O" and tablero[8] == "O"):
            bloquear_botones_gato()
            messagebox.showinfo("Ganador", "EL GANADOR ES: " + self.Jugador2)
        elif (tablero[0] == "O" and tablero[4] == "O" and tablero[8] == "O") or (tablero[2] == "O" and tablero[4] == "O" and tablero[6] == "O"):
            bloquear_botones_gato()
            messagebox.showinfo("Ganador", "EL GANADOR ES: " + self.Jugador2)

    boton_0=Button(ventana,width=10,height=4,command=lambda: Turnos(0))
    lista_De_Botones.append(boton_0)
    boton_0.place(x=50,y=50)        #Cada boton se asigna a una coordenada en la ventana, de modo que quede con la forma del juego del gato

    boton_1=Button(ventana,width=10,height=4,command=lambda: Turnos(1))         #Las funciones lambda nos ayudan a que no se escriban numeros
    lista_De_Botones.append(boton_1)                                            #cuando se ejecute el programa
    boton_1.place(x=150,y=50)

    boton_2=Button(ventana,width=10,height=4,command=lambda: Turnos(2))
    lista_De_Botones.append(boton_2)
    boton_2.place(x=250,y=50)

    boton_3=Button(ventana,width=10,height=4,command=lambda: Turnos(3))
    lista_De_Botones.append(boton_3)
    boton_3.place(x=50,y=150)

    boton_4=Button(ventana,width=10,height=4,command=lambda: Turnos(4))
    lista_De_Botones.append(boton_4)
    boton_4.place(x=150,y=150)

    boton_5=Button(ventana,width=10,height=4,command=lambda: Turnos(5))
    lista_De_Botones.append(boton_5)
    boton_5.place(x=250,y=150)

    boton_6=Button(ventana,width=10,height=4,command=lambda: Turnos(6))
    lista_De_Botones.append(boton_6)
    boton_6.place(x=50,y=250)

    boton_7=Button(ventana,width=10,height=4,command=lambda: Turnos(7))
    lista_De_Botones.append(boton_7)
    boton_7.place(x=150,y=250)

    boton_8=Button(ventana,width=10,height=4,command=lambda: Turnos(8))
    lista_De_Botones.append(boton_8)
    boton_8.place(x=250,y=250)

    turno_etiqueta=Label(ventana,textvariable=turno_delJugador).place(x=120,y=20) #Aqui alternan los jugadores para que sea por turnos
    Iniciar=Button(ventana, bg='red', fg='white', text='Da click aquí para jugar', width=20, height=4, command=iniciarJuego) .place(x=120, y=350)

g1 = Gato(1,1)
g1.bloquear_botones_gato()
g1.iniciarJuego()
g1.verificar_ganador()
g1.Turnos(num=0)
ventana.mainloop()  # Indica a la interfaz que debe quedarse esperando a que los usuarios hagan algo
